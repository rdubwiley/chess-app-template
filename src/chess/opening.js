import ChessBoard from "chessboardjs-vue";  
import Chess from "chess.js/chess.js";

import store from '../store'

function initBoard(boardId, openingMoves, color) {
    var testString = "r1bqkb1r/pppp1ppp/2n2n2/4p3/4P3/3P1N2/PPPN1PPP/R1BQKB1R b KQkq - 2 4"
    var testPgn = "1. e4 e5 2. Nf3 Nc6 3. d3 Nf6 4. Nbd2 *"

    var testGame = new Chess()
    console.log(testGame.load_pgn(testPgn))
    console.log(testGame.history())

    function parseMoves(moves) {

    }

    var board = null
    var game = new Chess(testString)

    console.log(game.history())


    function onDragStart (source, piece, position, orientation) {
        // do not pick up pieces if the game is over
        if (game.game_over()) return false

        // only pick up pieces for White
        if (color==='white') {
            if (piece.search(/^b/) !== -1) return false
        }
        // only pick up pieces for black
        else {
            if (piece.search(/^w/) !== -1) return false
        }
        
    }

    function makeRandomMove () {
        var possibleMoves = game.moves()

        // game over
        if (possibleMoves.length === 0) return

        var randomIdx = Math.floor(Math.random() * possibleMoves.length)
        game.move(possibleMoves[randomIdx])
        board.position(game.fen())
    }

    function onDrop (source, target) {
        // see if the move is legal
        var move = game.move({
            from: source,
            to: target,
            promotion: 'q' // NOTE: always promote to a queen for example simplicity
        })

        // illegal move
        if (move === null) return 'snapback'

        // make random legal move for black
        window.setTimeout(makeRandomMove, 250)
    }

    function onSnapEnd () {
        board.position(game.fen())
    }

    var config = {
        draggable: true,
        position: 'start',
        onDragStart: onDragStart,
        onDrop: onDrop,
        onSnapEnd: onSnapEnd,
        orientation: color
    }
    
    var board = Chessboard(boardId, config)
    console.log(color)

    if (color==='black') {
        //window.setTimeout(makeRandomMove, 250)
    }
    board.position(testString)
    return board


}

export default initBoard